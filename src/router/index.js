import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/pages/Home';
import About from '@/components/pages/About';
import Services from "@/components/pages/Services";
import Contact from "@/components/pages/Contact";
import Experience from "@/components/pages/Experience";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/services',
      name: 'Services',
      component: Services
    },
    {
      path: '/experience',
      name: 'Experience',
      component: Experience
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    }
  ]
});
